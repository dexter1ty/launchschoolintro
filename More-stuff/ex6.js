let words = [
  'laboratory',
  'experiment',
  'flab',
  'Pans Labyrinth',
  'elaborate',
  'polar bear',
];

function allMatches(wordsArr,regexp) {
  return wordsArr.filter(word => regexp.test(word));
}

console.log(allMatches(words, /lab/));