// ex. 3  

let rl = require('readline-sync');
let age = Number(rl.question('What is your age?'));

console.log(`You are ${age} years old.`);
console.log(`In 10 years, you will be ${age + 10} years old.`);
console.log(`In 20 years, you will be ${age + 20} years old.`);
console.log(`In 30 years, you will be ${age + 30} years old.`);

// ex. 1 of chapter Loops

console.log(`You are ${age} years old.`);


for(let i = 1 ; i < 4 ; ++i) {
  console.log(`In ${i * 10} years, you will be ${age + i * 10} years old.`); 
}