// ex. 1

const name = 'Victor';

console.log("Good Morning, " + name + ".");
console.log("Goof Afternoon, " + name + ".");
console.log("Good Evening, " + name + ".\n");

// ex. 2

const age = 20;

console.log(`You are ${age + 10} years old.`);
console.log(`In 10 years, you will be ${age + 10} years old.`);
console.log(`In 20 years, you will be ${age + 20} years old.`);
console.log(`In 30 years, you will be ${age + 30} years old.`);

// ex. 3

// not initialized

// ex. 5

let foo = 'bar';

{
  let foo = 'rab';
}

console.log(foo);

{
  foo = 'rab';
}

console.log(foo);




