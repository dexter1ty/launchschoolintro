function creatObjCopy(obj,arr){
  if(!arr) {
    return obj;
  }
  let newObj = {};
  arr.forEach(element => {
    newObj[element] = obj[element];
  });
  return newObj;
}

let objToCopy = {
  foo: 1,
  bar: 2,
  qux: 3,
};

let myNewObj = creatObjCopy(objToCopy, ['foo'])

console.dir(myNewObj);
