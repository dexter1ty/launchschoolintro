// Fibonacci

function fibo(number) {
  if(number < 2) return number;
  return fibo(number - 1) + fibo(number - 2);
}


// ex. 1 

function factorial(num) {
  let result = 1; 
  for(i = 1 ; i <= num ; i += 1) {
    result *= i;
  }

  return result;
}

console.log(factorial(8));

// ex. 5
console.log('==========================');


function randomNumberBetween(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

let tries = 0;
let result;

do {
  result = randomNumberBetween(1, 6);
  tries += 1;
  console.log(result);
} while (result <= 2);

console.log('It took ' + String(tries) + ' tries to get a number greater than 2');

// ex. 6
console.log('==========================');

function rFactorial(num) {
  if(num < 2) return num;
  return rFactorial(num - 1 ) * num
}

console.log(rFactorial(4));