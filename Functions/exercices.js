// ex. 3

function getNumber(prompt) {
  let rl = require('readline-sync');
  let num = rl.question(prompt);
  return num;
}

const multiply = (num1, num2) => {
  let result = num1 * num2;
  return result;
}

num1 = getNumber('Enter number one: ');
num2 = getNumber('Enter number two: ');

console.log(`${num1} * ${num2} = ${multiply(num1,num2)}`);

