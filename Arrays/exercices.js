let a = [1, 2, 3];
let b = a;
console.log(a === b);
a = [];
console.log(a === b);

console.log("\n========================\n");

const comparArr = (arr1, arr2) => {
  if(arr1.length !== arr2.length) return false;
  
  for(let i = 0; i < arr1.length; i += 1) {
    if(arr1[i] !== arr2[i]) return false;
  }

  return true;
}

arr = [1,6,4,2,9,7,0,3];
arr.sort()
console.log(arr.join(' '));

console.log("\n========================\n");

// ex.1 

/*

let array1 = [1, 2, undefined, 4]; length = 4;

let array2 = [1];
array2.length = 5;  length = 5;

let array3 = [];
array3[-1] = [1];  length = 0;

let array4 = [1, 2, 3, 4, 5];
array4.length = 3;  length = 3;

let array5 = [];
array5[100] = 3;  length = 1;

*/

// ex. 2

let myArray = [1, 3, 6, 11, 4, 2, 4, 9, 17, 16, 0];

myArray.forEach(value => {
  if(value % 2 === 0) console.log(value);
});

myArray = [
  [1, 3, 6, 11],
  [4, 2, 4],
  [9, 17, 16, 0],
];

console.log("\n========================\n");

myArray.forEach(nArray => {
  nArray.forEach(value => {
    if(value % 2 === 0) console.log(value);
  });
});

console.log("\n========================\n");

myArray = [1, 3, 6, 11, 4, 2, 4, 9, 17, 16, 0];

let newArray = myArray.map(value => {
  if(value % 2 === 0) {
    return 'even';
  } else {
    return 'odd';
  }
});

console.log(newArray);

console.log("\n========================\n");

// ex. 5

let things = [1, 'a', '1', 3, NaN, 3.1415, -4, null, false];

let findIntegers = array => {
  return array.filter(value => Number.isInteger(value));;
}

console.log(findIntegers(things));

console.log("\n========================\n");

// ex. 6

const oddLengths = strings => {
  return strings.map(value => value.length).filter(value => value % 2 !== 0);
}

let stringsArr = ['a', 'abcd', 'abcde', 'abc', 'ab'];
console.log(oddLengths(stringsArr));

console.log("\n========================\n");

// ex. 7

let arr2Square = [3, 5, 7];

const sumOfSquares = array => {
  return array.reduce((accumulator, currentValue) => accumulator + currentValue * currentValue, 0);
}

console.log(sumOfSquares(arr2Square));

console.log("\n========================\n");

// ex. 8

const reducedOddLengths = array => {
  return array.reduce((filteredArray, currentValue) => {
    if(currentValue.length % 2 !== 0) {
      filteredArray.push(currentValue.length);
    }

    return filteredArray;
  },[]);
}

console.log(reducedOddLengths(stringsArr));

console.log("\n========================\n");

// ex. 9

function check3(arr){
  return arr.includes(3);
}

let numbers1 = [1, 3, 5, 7, 9, 11];
let numbers2 = [];
let numbers3 = [2, 4, 6, 8];

console.log(check3(numbers1));
console.log(check3(numbers2));
console.log(check3(numbers3));

console.log("\n========================\n");

// ex. 10

let arraytoCheck = [
  ["hello", "world"],
  ["example", "mem", null, 6, 88],
  [4, 8, 12]
];

for (let i = 0 ; i < arraytoCheck.length ; i += 1){
  for (let j = 0 ; j < arraytoCheck[i].length ; j += 1){
    if(arraytoCheck[i][j] === 6){
      arraytoCheck[i][j] = 606;
    }
  }
}

console.log(arraytoCheck.flat());
