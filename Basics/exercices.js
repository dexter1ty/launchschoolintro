// ex. 1

'Clement ' + 'Engelberts';

// ex. 2

// ?

// ex. 3

console.log(typeof 'true');
console.log(typeof false);
console.log(typeof 1.5);
console.log(typeof 2);
console.log(typeof undefined);
console.log(typeof { foo: 'bar' });

// ex. 5

console.log(Number('5') + 10);

// ex. 6

console.log(`The value of 5 + 10 is ${Number('5') + 10}`);

// ex. 8

const array = ['a', 'b', 'c'];

// ex. 9

const pets = {
  asta: 'dog',
  pudding: 'cat',
}

