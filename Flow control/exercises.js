// ex. 1

/*

false || (true && false); = false
true || (1 + 2); = true
(1 + 2) || true; = true
true && (1 + 2); = true
false && (1 + 2); = false
(1 + 2) && true; = true
(32 * 4) >= 129; = false
false !== !true; = false
true === 4; = false
false === (847 === '847'); = true 
false === (847 == '847'); = false
(!true || (!(100 / 5) === 20) || ((328 / 4) === 82)) || false; = true

*/

// ex. 2 & 3

let evenOrOdd = (number) => {
  if(!Number.isInteger(number)) {
    console.log('The value you passed is not an integer');
    return;
  } 
  
  if(number % 2 === 0) {
    console.log('even');
  } else {
    console.log('odd');
  }
}

evenOrOdd(4.1);

// ex. 5

/*

if(foo()) {
  return 'bar'
} else {
  return qux();
}

*/

// ex. 7

let toCaps = (theString) => {
  if(theString.length < 10){
    return theString.toUpperCase();
  } else {
    return theString;
  }
}

console.log(toCaps('je suis'));

// ex. 8

function numberRange(num) {
  if(num >= 0 && num < 51) {
    console.log(`${num} is between 0 and 50.`);
  } else if(num > 50 && num < 101) {
    console.log(`${num} is between 51 and 100.`);
  } else if(num > 100) {
    console.log(`${num} is greater than 100.`);
  } else {
    console.log(`${num} is less than 0.`)
  }
}

numberRange(-2);




